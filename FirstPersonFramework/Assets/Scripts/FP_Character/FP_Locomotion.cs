﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class FP_Locomotion : MonoBehaviour
{
    #region Components
    private CharacterController _characterController;
    #endregion Components

    #region Input
    private Vector2 _movementInput;
    #endregion Input

    #region General Movement
    private bool _canMove = true;
    public bool CanMove
    {
        get { return _canMove; }
    }
    #endregion General Movement

    #region Camera
    [Header("Camera")]
    [SerializeField] private Transform _cameraTransform;
    #endregion Camera

    #region Walking
    [Header("Walking")]
    [SerializeField] private float _regularWalkingSpeed = 3f;
    [SerializeField] private float _sprintSpeed = 10f;
    [SerializeField] private float _crouchedWalkingSpeed = 2f;
    private float _realSpeed;
    private float _currentWalkingSpeed;
    private bool _wantsToSprint = false;
    #endregion Walking

    #region Stamina
    [Header("Stamina")]
    [SerializeField] private float _maxStamina = 100f;
    [Tooltip("Stamina decay per second")]
    [SerializeField] private float _staminaDecay = 10f;
    [Tooltip("Stamina regenerated per second")]
    [SerializeField] private float _staminaRegeneration = 15f;
    private float _currentStamina;
    #endregion Stamina

    #region Jumping & Gravity
    [Header("Jumping & Gravity")]
    [SerializeField] private float _jumpSpeed = 15f;
    [SerializeField] private float _gravityMultiplier = 1f;
    [SerializeField] private float _airControl = 0.25f;
    private bool _wantsToJump;

    private Vector3 _jumpVector;
    #endregion Jumping & Gravity

    #region Falling
    private float _fallSpeed;
    private bool _previouslyGrounded = true;
    private bool _isFalling = false;
    #endregion

    #region Crouching
    [Header("Crouching")]
    [SerializeField] bool _enableTapToggleCrouching = false;
    [SerializeField] float _crouchTransitionSpeed = 10f;

    private bool _wantsToCrouch = false;
    private bool _isCrouching = false;
    private float _standingCapsuleHeight;
    private float _standingCameraHeight;
    private float _crouchedCapsuleHeight;
    private float _crouchedCameraHeight;
    #endregion Crouching

    #region Toggle Features
    [Header("Toggle Features")]
    [SerializeField] private bool _enableSprinting = true;
    [SerializeField] private bool _enableJumping = true;
    [SerializeField] private bool _enableCrouching = true;
    #endregion Toggle Features

    #region Accessors
    public int CurrentSpeed
    {
        get
        {
            return Convert.ToInt32(
                new Vector2(_characterController.velocity.x, _characterController.velocity.z).magnitude
                );
        }
    }

    public int CurrentStamina
    {
        get
        {
            return Convert.ToInt32(_currentStamina);
        }
    }
    #endregion Accessors

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _standingCapsuleHeight = _characterController.height;
        _standingCameraHeight = _cameraTransform.localPosition.y;
        _crouchedCapsuleHeight = _standingCapsuleHeight / 2f;
        _crouchedCameraHeight = _standingCameraHeight / 2f;
        _currentStamina = _maxStamina;
    }

    // Start is called before the first frame update
    void Start()
    {
        _currentWalkingSpeed = _regularWalkingSpeed;
        _realSpeed = _currentWalkingSpeed;
    }

    void Update()
    {
        UpdateStamina();
        UpdateWalkingSpeed();
    }

    private void UpdateWalkingSpeed()
    {
        if (_wantsToSprint)
        {
            _realSpeed = _sprintSpeed;
        }
        else
        {
            _realSpeed = _regularWalkingSpeed;
        }

        if (!_isCrouching)
        {
            _currentWalkingSpeed = _realSpeed;
        }
    }

    public void OnSprint(InputAction.CallbackContext context)
    {
        if (!_enableSprinting) return;

        if (context.started)
        {
            _wantsToSprint = true;
        }
        else if (context.canceled)
        {
            _wantsToSprint = false;
        }
    }

    private void UpdateStamina()
    {
        Vector2 velocity2D = new Vector2(_characterController.velocity.x,
            _characterController.velocity.z);

        if (_wantsToSprint && !_isCrouching && velocity2D.magnitude != 0f)
        {
            _currentStamina = Mathf.Max(0f, _currentStamina - _staminaDecay * Time.deltaTime);
        }
        else
        {
            _currentStamina = Mathf.Min(_maxStamina, _currentStamina + _staminaRegeneration * Time.deltaTime);
        }

        if (_currentStamina == 0f)
        {
            _wantsToSprint = false;
        }
    }

    public void OnCrouch(InputAction.CallbackContext context)
    {
        if (!_enableCrouching) return;

        if (_enableTapToggleCrouching)
        {
            TapToggleCrouching(context);
        }
        else
        {
            RegularCrouching(context);
        }
    }

    private void RegularCrouching(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            _wantsToCrouch = true;
        }
        else if (context.canceled)
        {
            _wantsToCrouch = false;
        }
    }

    private void TapToggleCrouching(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            _wantsToCrouch = !_wantsToCrouch;
        }
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        if (!_canMove) return;

        _movementInput = context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        if (!_enableJumping) return;

        if (_characterController.isGrounded)
        {
            _wantsToJump = true;
        }
    }

    private void FixedUpdate()
    {
        UpdateCrouch();
        UpdateMovement();
        if (_characterController.isGrounded && !_previouslyGrounded)
        {
            Debug.Log("Hit ground at Y velocity of " + Mathf.Abs(_fallSpeed) + " m/s");
            // TODO: write function to handle fall damage
            _fallSpeed = 0;
            _isFalling = false;
        }
        _previouslyGrounded = _characterController.isGrounded;
    }

    private void UpdateMovement()
    {
        Vector3 movement = _movementInput.x * transform.right + _movementInput.y * transform.forward;
        movement *= _currentWalkingSpeed;

        if (!_characterController.isGrounded)
        {
            movement *= _airControl;
            if (_characterController.velocity.y < Mathf.Epsilon)
            {
                _fallSpeed = Mathf.Abs(_characterController.velocity.y);
                _isFalling = true;
            }
        }

        movement += Physics.gravity * _gravityMultiplier;

        if (_characterController.isGrounded)
        {
            _jumpVector = Vector3.Lerp(_jumpVector, Vector3.zero, 5 * Time.fixedDeltaTime);
            if (_wantsToJump)
            {
                _jumpVector = movement;
                _jumpVector.y = _jumpSpeed;
                _wantsToJump = false;
            }
        }
        else
        {
            _jumpVector += Physics.gravity * _gravityMultiplier * 1.25f * Time.fixedDeltaTime;
        }

        movement += _jumpVector;
        movement *= Time.fixedDeltaTime;
        _characterController.Move(movement);
    }

    void UpdateCrouch()
    {
        IsBlockedCrouching();
        if (_wantsToCrouch)
        {
            UpdateCapsuleHeight(_crouchedCapsuleHeight);
            UpdateCameraHeight(_crouchedCameraHeight);
            UpdateCrouchedWalkingSpeed(_crouchedWalkingSpeed);
            _isCrouching = true;
        }
        else if (_isCrouching == true && !IsBlockedCrouching() && _isCrouching == true)
        {
            UpdateCapsuleHeight(_standingCapsuleHeight);
            UpdateCameraHeight(_standingCameraHeight);
            UpdateCrouchedWalkingSpeed(_realSpeed);

            // Are we pretty much standing?
            if (Mathf.Approximately(_characterController.height, _standingCapsuleHeight))
            {
                _characterController.height = _standingCapsuleHeight;

                Vector3 capsuleCenter = _characterController.center;
                _characterController.center = capsuleCenter;

                _currentWalkingSpeed = _realSpeed;
                _isCrouching = false;
            }
        }
    }

    private bool IsBlockedCrouching()
    {
        float castDistance = _standingCapsuleHeight / 2f - _characterController.radius - 0.1f;
        Ray ray = new Ray(transform.position, transform.up);

        return Physics.SphereCast(ray, _characterController.radius, castDistance);
    }

    private void UpdateCrouchedWalkingSpeed(float targetSpeed)
    {
        _currentWalkingSpeed = Mathf.Lerp(_currentWalkingSpeed, targetSpeed,
            _crouchTransitionSpeed * Time.fixedDeltaTime);
    }

    private void UpdateCameraHeight(float targetCameraHeight)
    {
        Vector3 currentCameraPosition = _cameraTransform.localPosition;
        float currentCameraHeight = currentCameraPosition.y;

        currentCameraHeight = Mathf.Lerp(currentCameraHeight, targetCameraHeight,
            _crouchTransitionSpeed * Time.fixedDeltaTime);
        currentCameraPosition.y = currentCameraHeight;

        _cameraTransform.localPosition = currentCameraPosition;
    }

    private void UpdateCapsuleHeight(float targetCapsuleHeight)
    {
        _characterController.height = Mathf.Lerp(_characterController.height, targetCapsuleHeight,
            _crouchTransitionSpeed * Time.fixedDeltaTime);
    }
}