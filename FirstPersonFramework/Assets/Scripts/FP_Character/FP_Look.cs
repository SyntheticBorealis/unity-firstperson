﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FP_Look : MonoBehaviour
{
    #region Mouse Look Settings
    [Header("Mouse Look Settings")]
    [SerializeField] private Vector2 _mouseSensitivity = new Vector2(1f, 1f);
    [SerializeField] private bool _enableMouseSmoothing = true;
    [SerializeField] private float _smoothTime = 5f;
    [SerializeField] private float _minimumXAngle = -90f;
    [SerializeField] private float _maximumXAngle = 90f;
    #endregion Mouse Look Settings

    private Transform _characterTransform;
    private FP_Locomotion _playerLocomotion;
    private Vector2 _mouseDelta;
    private Quaternion _targetCameraRotation;
    private Quaternion _targetCharacterRotation;

    void Awake()
    {
        _characterTransform = transform.parent;
        if (_characterTransform != null)
        {
            _playerLocomotion = _characterTransform.GetComponent<FP_Locomotion>();
            _targetCameraRotation = transform.localRotation;
            _targetCharacterRotation = transform.localRotation;

        }
        else
        {
            Debug.LogError("FP_Look: Parent object is null!");
        }
    }

    void FixedUpdate()
    {
        if (_playerLocomotion != null && _playerLocomotion.CanMove)
        {
            UpdateLook();
        }
    }

    private void UpdateLook()
    {
        float yRotation = _mouseDelta.x * _mouseSensitivity.x;
        float xRotation = _mouseDelta.y * _mouseSensitivity.y;

        _targetCharacterRotation *= Quaternion.Euler(0, yRotation, 0);
        _targetCameraRotation *= Quaternion.Euler(-xRotation, 0, 0);
        _targetCameraRotation = ClampRotationAroundXAxis(_targetCameraRotation);

        if (_enableMouseSmoothing)
        {
            _characterTransform.localRotation = Quaternion.Slerp(_characterTransform.localRotation,
                _targetCharacterRotation, _smoothTime * Time.deltaTime);

            transform.localRotation = Quaternion.Slerp(transform.localRotation, _targetCameraRotation,
                _smoothTime * Time.deltaTime);
        }
        else
        {
            _characterTransform.localRotation = _targetCharacterRotation;
            this.transform.localRotation = _targetCameraRotation;
        }
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        if (_playerLocomotion && _playerLocomotion.CanMove)
        {
            _mouseDelta = context.ReadValue<Vector2>();
        }
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, _minimumXAngle, _maximumXAngle);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}
