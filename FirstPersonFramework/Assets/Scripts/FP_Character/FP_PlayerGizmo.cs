﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class FP_PlayerGizmo : MonoBehaviour
{
    [SerializeField] private Color _playerCapsuleColour = Color.black;
    [SerializeField] private Color _playerArrowColour = Color.red;

    private void OnDrawGizmos()
    {
        CharacterController characterController = GetComponent<CharacterController>();

        DebugExtension.DrawCapsule(transform.position + (characterController.height / 2f) * -transform.up,
            transform.position + (characterController.height / 2f) * transform.up,
            _playerCapsuleColour, characterController.radius);
        DebugExtension.DrawArrow(transform.position + characterController.radius * transform.forward,
            transform.forward * 0.5f, _playerArrowColour);
    }
}